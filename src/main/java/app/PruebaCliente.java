/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import comunicacion.SysCom;
import java.net.InetAddress;


public class PruebaCliente {
    
    public static void main(String[] args){
              
      Cliente c = new Cliente();
      c.init(2000); // 2000 es el puerto del CLIENTE, donde recibe mensaje
      
      c.enviarMensaje("hola", "127.0.0.1",3001);
      c.enviarMensaje("SALIR","127.0.0.1",3001);
      c.fin();
    }
}
