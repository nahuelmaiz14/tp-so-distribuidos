/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import comunicacion.SysCom;
import java.net.InetAddress;


public class Cliente {
    
    private SysCom sc;
    
    public Cliente(){
       this.sc = new SysCom();
    }
    
    public void init(int portserver){
       this.sc.Init(portserver);
    }
    
    public void fin(){
        this.sc.fin();
    }
    
    public void enviarMensaje(String mensaje, String direccionIP, int puerto){
      String datos = mensaje;
      InetAddress IP=null;
      try {
        IP = InetAddress.getByName(direccionIP);
      }
      catch(Exception e){
         System.out.println(e);
      }
      int port = puerto;
      sc.enviar(datos.getBytes(), IP, port);
      //System.out.println("Datos enviados");
    }
    
    
    
}
