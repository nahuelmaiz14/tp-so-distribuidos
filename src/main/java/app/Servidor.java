/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import comunicacion.SysCom;


public class Servidor extends Thread {
    
    private SysCom sc;
    private byte[] datosrecibidos;
    
    public Servidor(){
        this.sc = new SysCom();
    }
    
    public void fin(){
        this.interrupt();
        this.sc.fin();
    }
    
    public void run(){
      boolean salir = false;
      while(!salir){
         byte[] datos = this.sc.recibir();
         String strsalir = new String(datos);
         if(strsalir == "SALIR"){
             salir = true;
         }
         else{
             this.datosrecibidos = datos;
         }
      }  
    }
    
    public void init(int portserver){
        this.sc.Init(portserver);

        // para un servidor multi hilo
        //this.start();
        
        System.out.println( "servidor en escucha en puerto:"+sc.getServerPort() );
        byte[] datos = this.sc.recibir();
        //String strsalir = new String(datos);
        this.datosrecibidos = datos;
    }
    
    public byte[] GetData(){
        return this.datosrecibidos;
    }
    
}
